#!/bin/bash

# script to run SDGVM in parallel on 30 cores 
# designed to be run from within it's own directory

#HOME=/home/alp
MODEL=#HOME##MOD#
INPUT=#INPUT#

SPINDIR='../spin_accel/output'
OUTPUT=output

SITES=#HOME##DATA#/sites/global_1deg_CRUNCEPv7.dat
N_SITES=( 1980 625 620 610  615  515 515  515 500  510
          575  575 560 430  500  500 520  525 550  575
          530  630 540 540  455  445 490  510 400  400
          550  511   )

# start script
# clear output directories
rm -r ./$OUTPUT
rm -r ./src

# copy source code and compiled model run file
mkdir ./$OUTPUT
mkdir ./src
cp $MODEL/*.f    ./src
cp $MODEL/sdgvm0 ./src

# run loop 
s4=0
((e=${#N_SITES[*]}))
for s in `seq 1 $e` ;do
 ((s1=$s - 1))
 ((s3=1 + $s4))
 ((s4=$s4 + ${N_SITES[$s1]}))

 mkdir ./$OUTPUT/grid$s

 echo "./src/sdgvm0 $INPUT $SPINDIR/grid$s ./$OUTPUT/grid$s  $SITES $s3 $s4 > ./$OUTPUT/g${s}out.txt &"
       ./src/sdgvm0 $INPUT $SPINDIR/grid$s ./$OUTPUT/grid$s  $SITES $s3 $s4 > ./$OUTPUT/g${s}out.txt &

done

wait
