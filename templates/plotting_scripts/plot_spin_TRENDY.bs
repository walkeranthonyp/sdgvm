#!/bin/bash

SRCDIR=#HOME##MOD#/tools
ARGS="of<-'#PROJECT#_spin' sim<-c(#S#) styr<-1 endyr<-#SPINNY# yr_mean<-c(#SPINNY#-9,#SPINNY#) project<-'#PROJECT#'"


cd $SRCDIR
# dynamic variables
ALLARGS="${ARGS} via<-c(2,3,8,15,19,20) pyear<-1"
Rscript plot_maps_lattice.R ${ALLARGS} 

# static land cover
ALLARGS="${ARGS} via<-24:32 cov_fixed<-T"
Rscript plot_maps_lattice.R ${ALLARGS} 



### END ###
