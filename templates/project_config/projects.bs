#!/bin/bash

# Sets up an SDGVM project, setup for a specific project within the broad class of simulations defined by runtype
# called by setup_SDGVM_project.bs 
# expects SDGVM_PROJECT to exist as an environment variable

# to add a new project simply add an additional case to the below case statement 

if [ $SDGVM_PROJECT == 'TRENDYv9' ]; then

  # For TRENDYv9
  SDGVM_CLIMSTY=1901
  SDGVM_SIMSTY=1700
  SDGVM_SIMENDY=2019
  SDGVM_OUTSTY=1900

  # data directories 
  SDGVM_CLIMDIR="crujra_2020"           
  SDGVM_LANDDIR="ESACCILCP2014_LUH2v2h_2020"           

elif [ $SDGVM_PROJECT == 'TRENDYv8' ]; then

  # For TRENDYv8
  # Default specific time variables 
  SDGVM_CLIMSTY=1901
  SDGVM_SIMSTY=1700
  SDGVM_SIMENDY=2018
  SDGVM_OUTSTY=1900

elif [ $SDGVM_PROJECT == 'TRENDYv7' ]; then

  # For TRENDYv7
  SDGVM_CLIMSTY=1901
  SDGVM_SIMSTY=1700
  SDGVM_SIMENDY=2017
  SDGVM_OUTSTY=1900

  # data directories 
  SDGVM_CLIMDIR="crujra_2018"           
  SDGVM_LANDDIR="ESACCILCP2014_LUH2v2h.1"           

else
  echo ""
  echo "No project: ${SDGVM_PROJECT}"
  echo "define your own in: ./projects.bs,"
  echo "for now defaults associated with runtype are used."
fi


# export variables
export SDGVM_CLIMSTY
export SDGVM_SIMSTY
export SDGVM_SIMENDY
export SDGVM_OUTSTY
export SDGVM_CLIMDIR
export SDGVM_LANDDIR



### END ###
