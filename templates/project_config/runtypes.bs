#!/bin/bash

# Sets up an SDGVM runtype, a broad class of simulations that have similar configurations 
# called by setup_SDGVM_project.bs 
# expects SDGVM_RUNTYPE to exist as an environment variable

# to add a new runtype simply add an additional case to the below case statement 

if [ $SDGVM_RUNTYPE == 'TRENDY' ]; then

  # For TRENDYv7
  # - and later TRENDY iterations

  # directory where template simulations are to be copied from 
  # - relative to "template" directory in model source code directory 
  SDGVM_CPRUN="master_mp"

  # Spinup 
  SDGVM_SPIN="spin_accel"
  SDGVM_SPINNY=500

  # Main simulation ensemble  
  SDGVM_MAIN_SIMS=( S0 S1 S2 S3 S4 S8 )
  
  # Default specific time variables that can be over-written when projects.bs is run
  SDGVM_CLIMSTY=1901
  SDGVM_SIMSTY=1700
  SDGVM_SIMENDY=2017
  SDGVM_OUTSTY=1900

  # Default data directories 
  SDGVM_CLIMDIR="crujra_2018"           
  SDGVM_LANDDIR="ESACCILCP2014_LUH2v2h.1"           


elif [ $SDGVM_RUNTYPE == 'scratch' ]; then

  # for user-defined runtypes

  # Spinup directory 
  SDGVM_SPIN="spin"
  SDGVM_SPINNY=500

  # directory where template simulations are to be copied from, relative to model source code 
  SDGVM_CPRUN=""

  # Main simulation ensemble  
  #MAIN_SIMS=( NC_zen0_beers C_zen0_beers C_zen_beers C_zen0_beersC C_zen_beersC NC_zen0_LP C_zen0_LP C_zen_LP  )
  #MAIN_SIMS=( Walker_N_ModA Walker_N_ModA_Tacc Kattge_ModA Kattge_ModA_Tacc  )
  #MAIN_SIMS=( Constant Constant_ModA Constant_ModA_Tacc LUNA LUNA_ModA LUNA_ModA_Tacc  )
  SDGVM_MAIN_SIMS=( tpu0 tpu1 tpu2 tpu3  )
  #MAIN_SIMS=( spin_accelv7 S0v7 S1v7 S3v7 spin_accelv7_npslim S0v7_npslim S1v7_npslim S3v7_npslim spin_accelv7_bamort S0v7_bamort S1v7_bamort S3v7_bamort )
  #MAIN_SIMS=( spin_accelv8 spin_shortv8 S0v8 S2p1v8 S2p1_2017_v8 S2p2v8 S2p3v8 )
  #MAIN_SIMS=( spin_accelv8 spin_shortv8 S0v8 S2p1v8 S2p1_2017_v8 S2p2v8 S2p3v8 )
  #SDGVM_MAIN_SIMS=( run ) 

  # Default specific time variables that can be over-written when projects.bs is run
  SDGVM_CLIMSTY=1901
  SDGVM_SIMSTY=1700
  SDGVM_SIMENDY=2019
  SDGVM_OUTSTY=1900

  # Default data directories 
  SDGVM_CLIMDIR="crujra_2020"           
  SDGVM_LANDDIR="ESACCILCP2014_LUH2v2h_2020"           


else
  echo ""
  echo "No runtype:"
  echo "  ${SDGVM_RUNTYPE}"
  echo "pick one of:"
  echo "  TRENDY, scratch" 
  echo "or define your own in: ./project_config/runtypes.bs"
  echo "SDGVM project setup ABORTED."
  exit 1
fi


# export variables
export SDGVM_SPIN 
export SDGVM_MAIN_SIMS
export SDGVM_CPRUN 
export SDGVM_SPINNY
export SDGVM_CLIMSTY
export SDGVM_SIMSTY
export SDGVM_SIMENDY
export SDGVM_OUTSTY
export SDGVM_CLIMDIR
export SDGVM_LANDDIR



### END ###
