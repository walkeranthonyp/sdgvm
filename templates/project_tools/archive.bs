#!/bin/bash


# project tool - tars and zips all SDGVM output in all run output directories
# - run from within the PROJECT tools directory

AFILE='archive.tar'
AFILEG='archive_grids.tar'

cd ..
DIRS=`ls --hide=*.* --hide=results --hide=tools`
echo $DIRS

for d in $DIRS; do
  echo $d/output
  cd $d/output

  # create full run archive
  tar -cf $AFILE --remove-files g*out.txt   
  tar -rf $AFILE grid*/simulation.dat   
  tar -rf $AFILE grid*/site_info.dat   
  tar -rf $AFILE grid*/diag.dat   
  tar -rf $AFILE --remove-files *.dat   
 
  # create grid output archives
  tar -cf $AFILEG --remove-files grid*/*.*   

  # zip archives 
  gzip -f $AFILE
  gzip -f $AFILEG
 
  cd ../../
done 



### END ###
