#!/bin/bash

SRCDIR=#HOME##MOD#/tools
ARGS="nsites<-#TSITES# dir<-'#DIR#' wd<-'#PDIR#' sim<-'#SIM#' sdgvmuser<-'#SDGVMUSER#'" 
cd $SRCDIR

# stich gridded output together
ALLARGS="${ARGS} ny<-319 sty<-1700 monthly<-F daily<-F netcdf<-F"
Rscript postprocess_mp.R ${ALLARGS}

# generate netcdf output
ALLARGS="${ARGS} ny<-319 sty<-1700 stich<-F netcdf<-T monthly<-F daily<-F " 
Rscript postprocess_mp.R ${ALLARGS}
ALLARGS="${ARGS} ny<-119 sty<-1900 stich<-F netcdf<-T monthly<-F daily<-F ncdf_avars<-c('cVegpft','landCoverFrac') "
Rscript postprocess_mp.R ${ALLARGS}
ALLARGS="${ARGS} ny<-319 sty<-1700 outsyear<-1900 stich<-F netcdf<-T monthly<-F daily<-F ncdf_avars<-c('fFire','fLuc') "
Rscript postprocess_mp.R ${ALLARGS}

# stich sub-annual gridded output together - monthly
ALLARGS="${ARGS} ny<-119 sty<-1900 netcdf<-F annual<-F daily<-F"
Rscript postprocess_mp.R ${ALLARGS}

# generate sub-annual netcdf output - monthly 
ALLARGS="${ARGS} ny<-119 syr<-1900 stich<-F netcdf<-T annual<-F daily<-F" 
Rscript postprocess_mp.R ${ALLARGS}



### END ###
