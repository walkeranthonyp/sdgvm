#!/bin/bash

SRCDIR=#HOME##MOD#/tools
ARGS="nsites<-#TSITES# grids<-#NP# cores<-#NP# fd<-'${SRCDIR}' dir<-'#DIR#' wd<-'#PDIR#' sim<-'#SIM#' email<-'#EMAIL#'" 
PARG="person<-'#NAME#'" 
IARG="institution<-'#INSTITUTION#'" 
TIMEARGS="ny<-#NY# sty<-#STY#"
TIMEARGSMON="ny<-#OUTNY# sty<-#OUTSTY#"


cd $SRCDIR
# stich gridded output together
ALLARGS="${ARGS} ${TIMEARGS} monthly<-F daily<-F netcdf<-F"
Rscript postprocess_mp.R ${ALLARGS}

# generate netcdf output
ALLARGS="${ARGS} ${TIMEARGS} stich<-F netcdf<-T monthly<-F daily<-F"
Rscript postprocess_mp.R ${ALLARGS} "$PARG" "$IARG"
ALLARGS="${ARGS} ${TIMEARGSMON} stich<-F netcdf<-T monthly<-F daily<-F ncdf_avars<-c('cVegpft','landCoverFrac') "
Rscript postprocess_mp.R ${ALLARGS} "$PARG" "$IARG"
ALLARGS="${ARGS} ${TIMEARGS} outsyear<-#OUTSTY# stich<-F netcdf<-T monthly<-F daily<-F ncdf_avars<-c('fFire','fLuc')"
Rscript postprocess_mp.R ${ALLARGS} "$PARG" "$IARG"

# stich sub-annual gridded output together - monthly
ALLARGS="${ARGS} ${TIMEARGSMON} netcdf<-F annual<-F daily<-F"
Rscript postprocess_mp.R ${ALLARGS}

# generate sub-annual netcdf output - monthly
ALLARGS="${ARGS} ${TIMEARGSMON} stich<-F netcdf<-T annual<-F daily<-F"
Rscript postprocess_mp.R ${ALLARGS} "$PARG" "$IARG"



### END ###
