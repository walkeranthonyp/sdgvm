#!/bin/bash

# CRUJRA sites
# 0.5 degree resolution
# 100 cores 


SDGVM_SITES=#HOME##DATA#/sites/gl_30min_CRUJRAv8.dat
SDGVM_NODES='' 
SDGVM_NP=100   # processors per node; if nodes empty total processors  
SDGVM_N_SITES=( 496 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676
                676 676 676 676 676 676 676 676 676 676 ) 
SDGVM_SITES_TOTAL=29761 # this is not sum(N_SITES) but is a combination of that and the SDGVM landmask, needs to be determined after a first run of SDGVM with a new sites list

export SDGVM_SITES
export SDGVM_NODES
export SDGVM_NP
export SDGVM_N_SITES
export SDGVM_SITES_TOTAL



### END ###
