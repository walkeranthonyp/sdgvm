#!/bin/bash

# CRUJRA sites
# 0.5 degree resolution
# 64 cores 


SDGVM_SITES=#HOME##DATA#/sites/gl_30min_CRUJRAv8.dat
SDGVM_NODES='' 
SDGVM_NP=64   # processors per node; if nodes empty total processors  
SDGVM_N_SITES=( 1081 1053 1053 1053 1053 1053 1053 1053 1053 1053
                1053 1053 1053 1053 1053 1053 1053 1053 1053 1053
                1053 1053 1053 1053 1053 1053 1053 1053 1053 1053
                1053 1053 1053 1053 1053 1053 1053 1053 1053 1053
                1053 1053 1053 1053 1053 1053 1053 1053 1053 1053
                1053 1053 1053 1053 1053 1053 1053 1053 1053 1053
                1053 1053 1053 1053 ) 
SDGVM_SITES_TOTAL=62220 # this is not sum(N_SITES) but is a combination of that and the SDGVM landmask, needs to be determined after a first run of SDGVM with a new sites list

export SDGVM_SITES
export SDGVM_NODES
export SDGVM_NP
export SDGVM_N_SITES
export SDGVM_SITES_TOTAL



### END ###      
